# STARTX SXCM Ansible

The purpose of this repository is to help you use the [`startxfr.sxcm` ansible collection](https://startx-ansible-sxcm.readthedocs.io)
to manage the lifecycle of multi-OKD/OCP clusters using a full infrastructure stack build around automation controller, terraform, vault and gitlab.

Refer to [latest documentation of the collection](https://startx-ansible-sxcm.readthedocs.io)
for more informations on how to configure and use this top level role.
