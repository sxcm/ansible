#!/bin/bash
# Global setting
DEBUG=no
reconstruct=no
removeruntime=no
# Local setting
sxcm_context_dir=$(pwd)
sxcm_sxfrcoll_dir="${sxcm_context_dir}/../../ansible-gitlab"
sxcm_debug="${DEBUG:-true}"
sxcm_debug_param=""
sxcm_pb_name="devel-startx.yml"
sxcm_container_image="quay.io/startx/sxas-ansible-ee"
cmd_inpod="podman exec -it sxcm-test bash -c "

# enable verbose if debug is true
if [[ "$sxcm_debug" == "yes" || "$sxcm_debug" == "true" ]]; then
       sxcm_debug_param="-vvv -e 'sxcm_debug=true'"
fi

if [[ "$reconstruct" == "yes" ]]; then
       echo "CONSTRUCT sxcm-test container"
       rm -rf /tmp/sxcm &>/dev/null && mkdir /tmp/sxcm &>/dev/null
       podman rm -vf sxcm-test &>/dev/null
       podman run -d --name sxcm-test \
              -v "${sxcm_context_dir}":/runner:Z \
              -v /tmp/sxcm:/tmp/sxcm:z \
              --security-opt label=disable \
              "${sxcm_container_image}" \
              sleep infinity
       echo "CONSTRUCT is re-installing requirements"
       $cmd_inpod "cd /runner && ansible-galaxy install -f -r /runner/requirements.yml --roles-path=~/.ansible/roles"
else
       echo "START sxcm-test container"
       podman start sxcm-test
fi
echo "START copying startxfr.* devel collections content to ${sxcm_context_dir}/ansible_collections/startxfr"
rm -rf "${sxcm_context_dir}/ansible_collections/startxfr" && \
mkdir -p "${sxcm_context_dir}/ansible_collections/startxfr" && \
cp -R "${sxcm_sxfrcoll_dir}"/* "${sxcm_context_dir}/ansible_collections/startxfr/"
echo "START running test playbook"
$cmd_inpod "cd /runner && ansible-playbook playbooks/${sxcm_pb_name} ${sxcm_debug_param}"

if [[ "$removeruntime" == "yes" ]]; then
       echo "REMOVE sxcm-test container"
       podman rm -vf sxcm-test
fi